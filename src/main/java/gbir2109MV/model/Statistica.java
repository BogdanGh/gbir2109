package gbir2109MV.model;

import java.util.*;

public class Statistica {

	private Map<String, Integer> intrebariDomenii;
	
	public Statistica() {
		intrebariDomenii = new HashMap<String, Integer>();
	}
	
	public void add(String key, Integer value){
		intrebariDomenii.put(key, value);
	}

	public Map<String, Integer> getIntrebariDomenii() {
		return intrebariDomenii;
	}

	public void setIntrebariDomenii(Map<String, Integer> intrebariDomenii) {
		this.intrebariDomenii = intrebariDomenii;
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		List<Map.Entry<String, Integer>> list =
				new LinkedList<Map.Entry<String, Integer>>(intrebariDomenii.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
							   Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Map.Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		for(String domeniu : sortedMap.keySet()){
			sb.append(domeniu + ": " + intrebariDomenii.get(domeniu) + "\n");
		}
		
		return sb.toString();
	}

}
