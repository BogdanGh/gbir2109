package gbir2109MV.controller;

import java.util.LinkedList;
import java.util.List;

import gbir2109MV.exception.InputValidationFailedException;
import gbir2109MV.model.Intrebare;
import gbir2109MV.model.Statistica;
import gbir2109MV.model.Test;
import gbir2109MV.repository.IntrebariRepository;
import gbir2109MV.exception.DuplicateIntrebareException;
import gbir2109MV.exception.NotAbleToCreateStatisticsException;
import gbir2109MV.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	private final String[] domenii = {"Matematica","Biologie", "Lb Romana", "Geografie"};

	public AppController(IntrebariRepository repo) {
		intrebariRepository = repo;
	}
	
	public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException{

		if(!domeniuIncluded(intrebare.getDomeniu()))
			return null;
		intrebariRepository.addIntrebare(intrebare);
		
		return intrebare;
	}

	public Intrebare Intrebare(String enunt, String varianta1, String varianta2, String varianta3,
							   String variantaCorecta, String domeniu) throws InputValidationFailedException {
		return new Intrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
	}

	private boolean domeniuIncluded(String domeniu){
		for ( String dom : domenii){
			if(dom.equals(domeniu))
				return true;
		}
		return false;
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		
		if(intrebariRepository.getNumberOfDistinctDomains() < 4)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(4)");
//
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
//		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 5){
			intrebare = intrebariRepository.pickRandomIntrebare();
			
			if(!testIntrebari.contains(intrebare) && domeniuIncluded(intrebare.getDomeniu())){
				testIntrebari.add(intrebare);
//				domenii.add(intrebare.getDomeniu());
			}
			
		}
		
		test.setIntrebari(testIntrebari);
		return test;
		
	}
	
	public void loadIntrebariFromFile(String f){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.add(domeniu, intrebariRepository.getIntrebariByDomain(domeniu).size());
		}
		
		return statistica;
	}

}
