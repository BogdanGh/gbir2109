package gbir2109MV.exception;

public class DuplicateIntrebareException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DuplicateIntrebareException(String message) {
		super(message);
	}

}
