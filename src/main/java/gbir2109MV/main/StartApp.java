package gbir2109MV.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

import gbir2109MV.exception.DuplicateIntrebareException;
import gbir2109MV.exception.InputValidationFailedException;
import gbir2109MV.exception.NotAbleToCreateTestException;
import gbir2109MV.model.Intrebare;
import gbir2109MV.model.Statistica;

import gbir2109MV.controller.AppController;
import gbir2109MV.exception.NotAbleToCreateStatisticsException;
import gbir2109MV.model.Test;
import gbir2109MV.repository.IntrebariRepository;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "D:\\Facultate\\Anul3\\Semestrul2\\VVSS\\Lab01\\src\\main\\java\\gbir2109MV\\repository\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		IntrebariRepository repo = new IntrebariRepository();
		AppController appController = new AppController(repo);
		appController.loadIntrebariFromFile(file);

		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				Scanner sc = new Scanner(System.in);
				System.out.println("Domeniu: ");
				String domeniu = sc.nextLine();
				System.out.println("Enunt: ");
				String enunt = sc.nextLine();
				System.out.println("Raspuns 1");
				String var1 = sc.nextLine();
				System.out.println("Raspuns 2");
				String var2 = sc.nextLine();
				System.out.println("Raspuns 3");
				String var3 = sc.nextLine();
				System.out.println("Raspunsul corect");
				String raspunsCorect = sc.nextLine();
				try {
					Intrebare intr = new Intrebare(enunt,var1,var2,var3,raspunsCorect,domeniu);
					appController.addNewIntrebare(intr);
				} catch (InputValidationFailedException e) {
					e.printStackTrace();
				} catch (DuplicateIntrebareException e) {
					e.printStackTrace();
				}
				break;
			case "2" :
				try {
					Test test = appController.createNewTest();
					List<Intrebare> intrebari = test.getIntrebari();
					for(Intrebare intr : intrebari){
						System.out.println(intr.getEnunt() + "\n" + intr.getVarianta1() + "\n" + intr.getVarianta2() + "\n" + intr.getVarianta3());
					}
				} catch (NotAbleToCreateTestException e) {
					e.printStackTrace();
				}
				break;
			case "3" :

				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					// TODO 
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
