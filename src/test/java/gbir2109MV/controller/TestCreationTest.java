package gbir2109MV.controller;

import gbir2109MV.exception.DuplicateIntrebareException;
import gbir2109MV.exception.InputValidationFailedException;
import gbir2109MV.exception.NotAbleToCreateTestException;
import gbir2109MV.model.Intrebare;
import gbir2109MV.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestCreationTest {
    private final String f = "src\\test\\java\\gbir2109MV\\controller\\test1";
    private final String f2 = "src\\test\\java\\gbir2109MV\\controller\\test2";
    private final String f3 = "src\\test\\java\\gbir2109MV\\controller\\test3";
    private final String f4 = "src\\test\\java\\gbir2109MV\\controller\\test4";

    private IntrebariRepository intrebariRepository;
    private IntrebariRepository intrebariRepository2;
    private IntrebariRepository intrebariRepository3;
    private IntrebariRepository intrebariRepository4;

    @Before
    public void initialization(){
        intrebariRepository = new IntrebariRepository();
        intrebariRepository2 = new IntrebariRepository();
        intrebariRepository3 = new IntrebariRepository();
        intrebariRepository4 = new IntrebariRepository();

//        intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
//        intrebariRepository2.setIntrebari(intrebariRepository2.loadIntrebariFromFile(f2));
//        intrebariRepository3.setIntrebari(intrebariRepository3.loadIntrebariFromFile(f3));
//        intrebariRepository4.setIntrebari(intrebariRepository4.loadIntrebariFromFile(f4));

        try {
            intrebariRepository.addIntrebare(new Intrebare("Test1?","1) a","2) b","3) c","1","Matematica"));

            intrebariRepository2.addIntrebare(new Intrebare("Test1?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository2.addIntrebare(new Intrebare("Test2?","1) a","2) b","3) c","2","Biologie"));
            intrebariRepository2.addIntrebare(new Intrebare("Test3?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository2.addIntrebare(new Intrebare("Test4?","1) a","2) b","3) c","2","Geografie"));
            intrebariRepository2.addIntrebare(new Intrebare("Test5?","1) a","2) b","3) c","3","Lb Romana"));
            intrebariRepository2.addIntrebare(new Intrebare("Test6?","1) a","2) b","3) c","3","Matematica"));

            intrebariRepository3.addIntrebare(new Intrebare("Test1?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository3.addIntrebare(new Intrebare("Test2?","1) a","2) b","3) c","2","Biologie"));
            intrebariRepository3.addIntrebare(new Intrebare("Test3?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository3.addIntrebare(new Intrebare("Test4?","1) a","2) b","3) c","2","Geografie"));
            intrebariRepository3.addIntrebare(new Intrebare("Test5?","1) a","2) b","3) c","3","Lb Romana"));
            intrebariRepository3.addIntrebare(new Intrebare("Test6?","1) a","2) b","3) c","3","Matematica"));

            intrebariRepository4.addIntrebare(new Intrebare("Test1?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository4.addIntrebare(new Intrebare("Test2?","1) a","2) b","3) c","2","Biologie"));
            intrebariRepository4.addIntrebare(new Intrebare("Test3?","1) a","2) b","3) c","1","Matematica"));
            intrebariRepository4.addIntrebare(new Intrebare("Test4?","1) a","2) b","3) c","2","Geografie"));
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TC_01() {
        try {
            AppController appController = new AppController(intrebariRepository);
            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(false);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void TC_02() {
        try {
            AppController appController = new AppController(intrebariRepository2);
            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(t.getIntrebari().size() == 5);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void TC_03() {
        try {
            AppController appController = new AppController(intrebariRepository3);
            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(t.getIntrebari().size()==5);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void TC_04() {
        try {
            AppController appController = new AppController(intrebariRepository4);
            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(false);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }
}
