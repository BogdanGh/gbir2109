package gbir2109MV.controller;

import gbir2109MV.exception.InputValidationFailedException;
import gbir2109MV.exception.NotAbleToCreateStatisticsException;
import gbir2109MV.model.Intrebare;
import gbir2109MV.model.Statistica;
import gbir2109MV.repository.IntrebariRepository;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerTest {

    @Test
    public void tc1_EC() {
        try {
            Intrebare intr1 = new Intrebare("Prima intrebare?", "1) A", "2) B", "3) C", "1", "Matematica");
            assertTrue(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(false);
        }


    }

    @Test
    public void tc2_EC() {
//        try {
////            Intrebare intr1 = new Intrebare("", "1) A", "2) B", "3) C", 1, "Matematica");
//            assertTrue(false);
//        } catch (InputValidationFailedException e) {
//            e.printStackTrace();
//            assertTrue(true);
//        }
    }

    @Test
    public void tc3_EC() {
//        try {
//            Intrebare intr1 = new Intrebare(3, "1) A", "2) B", "3) C", "1", "Matematica");
//            assertTrue(false);
//        } catch (InputValidationFailedException e) {
//            e.printStackTrace();
//            assertTrue(true);
//        }


    }

    @Test
    public void tc4_EC() {
        try {
            Intrebare intr1 = new Intrebare("Prima intrebare?", "1) A", "2) B", "3) C", "5", "Matematica");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void tc1_BVA() {
        try {
            String content = "";
            for(int i = 0 ; i<96;i++)
                content += "a";
            Intrebare intr1 = new Intrebare("P"+content+"?", "1) A", "2) B", "3) C", "1", "Matematica");
            assertTrue(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(false);
        }


    }

    @Test
    public void tc2_BVA() {
//        try {
//            Intrebare intr1 = new Intrebare(null, "1) A", "2) B", "3) C", 1, "Matematica");
//            assertTrue(false);
//        } catch (InputValidationFailedException e) {
//            e.printStackTrace();
//            assertTrue(true);
//        }
    }

    @Test
    public void tc3_BVA() {
        try {
            Intrebare intr1 = new Intrebare("P?", "1) A", "2) B", "3) C", "1", "Matematica");
            assertTrue(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(false);
        }


    }

    @Test
    public void tc4_BVA() {
        try {
            String content = "";
            for(int i = 0 ; i<96;i++)
                content += "a";
            Intrebare intr1 = new Intrebare("P"+content+"?", "1) A", "2) B", "3) C", "11", "Matematica");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void func3_EC_Nevalid(){
        try {
            IntrebariRepository repo = new IntrebariRepository();
            repo.setIntrebari(repo.loadIntrebariFromFile("D:\\Facultate\\Anul3\\Semestrul2\\VVSS\\Lab01\\src\\test\\java\\gbir2109MV\\controller\\empty"));
            AppController appController = new AppController(repo);

            Statistica st = appController.getStatistica();
            assertTrue(false);
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void func3_EC_Valid(){
        try {
            IntrebariRepository repo = new IntrebariRepository();
            repo.setIntrebari(repo.loadIntrebariFromFile("D:\\Facultate\\Anul3\\Semestrul2\\VVSS\\Lab01\\src\\test\\java\\gbir2109MV\\controller\\test3"));
            AppController appController = new AppController(repo);

            Statistica st = appController.getStatistica();
            assertTrue(st.getIntrebariDomenii().keySet().size() == 4);
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }
}