package gbir2109MV.controller;

import gbir2109MV.exception.DuplicateIntrebareException;
import gbir2109MV.exception.InputValidationFailedException;
import gbir2109MV.exception.NotAbleToCreateStatisticsException;
import gbir2109MV.exception.NotAbleToCreateTestException;
import gbir2109MV.model.Intrebare;
import gbir2109MV.model.Statistica;
import gbir2109MV.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class IncrementalIntegration {

    private AppController appController;
    private IntrebariRepository repo;
    @Before
    public void initialization(){
         repo = new IntrebariRepository();
        try {
            repo.addIntrebare(new Intrebare("Test1?","1) a","2) b","3) c","1","Matematica"));

        repo.addIntrebare(new Intrebare("Test2?","1) a","2) b","3) c","2","Biologie"));
        repo.addIntrebare(new Intrebare("Test3?","1) a","2) b","3) c","1","Matematica"));
        repo.addIntrebare(new Intrebare("Test4?","1) a","2) b","3) c","2","Geografie"));
        repo.addIntrebare(new Intrebare("Test5?","1) a","2) b","3) c","3","Lb Romana"));
            repo.addIntrebare(new Intrebare("Test6?","1) a","2) b","3) c","3","Matematica"));

        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }
        //repo.setIntrebari(repo.loadIntrebariFromFile("src\\test\\java\\gbir2109MV\\controller\\test3"));
        appController = new AppController(repo);
    }

    @Test
    public void intrebare() {
        try {
            Intrebare intr1 = new Intrebare("Prima intrebare?", "1) A", "2) B", "3) C", "1", "Matematica");
            assertTrue(intr1.getEnunt().equals("Prima intrebare?"));
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void func2_TC() {
        try {
            String f2 = "src\\test\\java\\gbir2109MV\\controller\\test2";
            IntrebariRepository intrebariRepository2 = new IntrebariRepository();
            intrebariRepository2.setIntrebari(intrebariRepository2.loadIntrebariFromFile(f2));

            AppController appController = new AppController(intrebariRepository2);
            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(t.getIntrebari().size() == 5);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void func3_EC_Valid(){
        try {
            IntrebariRepository repo = new IntrebariRepository();
            repo.setIntrebari(repo.loadIntrebariFromFile("src\\test\\java\\gbir2109MV\\controller\\test3"));
            AppController appController = new AppController(repo);

            Statistica st = appController.getStatistica();
            assertTrue(st.getIntrebariDomenii().keySet().size() == 4);
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void int1(){
        int initialSize = repo.getIntrebari().size();

        try {
            Intrebare intr1 = new Intrebare("Prima intrebare?", "1) A", "2) B", "3) C", "1", "Matematica");
            appController.addNewIntrebare(intr1);

            assertTrue(repo.getIntrebari().size() == initialSize + 1);
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            assertTrue(false);
            e.printStackTrace();
        }
    }

    @Test
    public void int2(){
        int initialSize = repo.getIntrebari().size();

        try {
            Intrebare intr1 = new Intrebare("Prima intrebare, sau poate a doua?", "1) A", "2) B", "3) C", "1", "Matematica");
            appController.addNewIntrebare(intr1);

            assertTrue(repo.getIntrebari().size() == initialSize + 1);

            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(t.getIntrebari().size() == 5);
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            assertTrue(false);
            e.printStackTrace();
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void int3(){
        int initialSize = repo.getIntrebari().size();

        try {
            Intrebare intr1 = new Intrebare("Prima intrebare, sau poate a doua, sau a treia?", "1) A", "2) B", "3) C", "1", "Matematica");
            appController.addNewIntrebare(intr1);
            assertTrue(repo.getIntrebari().size() == initialSize + 1);

            gbir2109MV.model.Test t = appController.createNewTest();
            assertTrue(t.getIntrebari().size() == 5);

            Statistica st = appController.getStatistica();
            assertTrue(st.getIntrebariDomenii().keySet().size() == 4);
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            assertTrue(false);
            e.printStackTrace();
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
    }
}
